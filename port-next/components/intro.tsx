'use client'

import Image from 'next/image'
import MyPhoto from '@/public/myPic.jpg'
import React, { useEffect, useRef } from 'react'
import { motion } from 'framer-motion'
import Link from 'next/link'
import { BsArrowRight, BsLinkedin } from 'react-icons/bs'
import { HiDownload } from 'react-icons/hi'
import { FaGithubSquare } from 'react-icons/fa'
import { useSectionInView } from '@/lib/hooks'
import { useActiveSectionContext } from '@/context/active-section-context'
import Typed from 'typed.js'
import { professions } from '@/lib/data'

export default function Intro() {
  const { ref } = useSectionInView('Home', 0.5)
  const { setActiveSection, setTimeOfLastClick } = useActiveSectionContext()

  const typedRef = useRef<HTMLInputElement>(null)
  const imgContainerRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    // makeAnimCard()
    const typed = new Typed(typedRef.current, {
      strings: professions,
      typeSpeed: 100,
      loop: true,
    })

    return () => {
      typed.destroy()
    }
  }, [])

  return (
    <section
      ref={ref}
      id="home"
      className="sm:flex gap-4 items-center w-[80%] mb-28 text-center sm:mb-0 scroll-mt-[100rem]"
    >
      <div
        ref={imgContainerRef}
        className="sm:min-w-[28%] sm:w-[66%] flex justify-center img-container"
      >
        <motion.div
          initial={{ opacity: 0, scale: 0 }}
          animate={{ opacity: 1, scale: 1 }}
          transition={{
            type: 'tween',
            duration: 0.2,
          }}
        >
          <Image
            src={MyPhoto}
            alt="Ricardo portrait"
            quality="100"
            priority={true}
            className="object-cover border-[0.35rem] border-white shadow-md sm:grayscale hover:grayscale-0 transition-all duration-300"
          />
        </motion.div>
      </div>
      <div className="glow" />

      <div className="sm:flex sm:justify-around sm:flex-col sm:text-left">
        <motion.h1
          className="mb-10 mt-4 px-4 text-2xl font-medium !leading-[1.5] sm:text-4xl "
          initial={{ opacity: 0, y: 100 }}
          animate={{ opacity: 1, y: 0 }}
        >
          <span className="font-bold">Hello, I'm Mukesh.</span>
          <br></br>I'm a <span className="font-bold" ref={typedRef}></span>
          <br></br>with <span className="font-bold">Creative Ideas</span> in
          technology. I enjoy building{' '}
          <span className="italic">sites & apps</span>. Let's have a quick{' '}
          <span className="underline cursor-pointer">
            <Link href="#contact">Chat</Link>
          </span>
          .
        </motion.h1>

        <motion.div
          className="flex flex-col sm:flex-row items-center gap-4 px-4 text-lg font-medium"
          initial={{ opacity: 0, y: 100 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{
            delay: 0.1,
          }}
        >
          <Link
            href="#contact"
            className="group bg-gray-900 text-white px-7 py-3 flex items-center gap-2 rounded-full outline-none focus:scale-110 hover:scale-110 hover:bg-gray-950 active:scale-105 transition"
            onClick={() => {
              setActiveSection('Contact')
              setTimeOfLastClick(Date.now())
            }}
          >
            Contact me here{' '}
            <BsArrowRight className="opacity-70 group-hover:translate-x-1 transition" />
          </Link>

          <a
            className="group bg-white px-7 py-3 flex items-center gap-2 rounded-full outline-none focus:scale-110 hover:scale-110 active:scale-105 transition cursor-pointer borderBlack dark:bg-white/10"
            href="/MukeshSutharResume.pdf"
            download
          >
            Download CV{' '}
            <HiDownload className="opacity-60 group-hover:translate-y-1 transition" />
          </a>

          <a
            className="bg-white p-4 text-gray-700 hover:text-gray-950 flex items-center gap-2 rounded-full focus:scale-[1.15] hover:scale-[1.15] active:scale-105 transition cursor-pointer borderBlack dark:bg-white/10 dark:text-white/60"
            href="https://www.linkedin.com/in/mukesh-suthar-6553a9160/"
            target="_blank"
          >
            <BsLinkedin />
          </a>

          <a
            className="bg-white p-4 text-gray-700 flex items-center gap-2 text-[1.35rem] rounded-full focus:scale-[1.15] hover:scale-[1.15] hover:text-gray-950 active:scale-105 transition cursor-pointer borderBlack dark:bg-white/10 dark:text-white/60"
            href="https://github.com/mukeshs1510"
            target="_blank"
          >
            <FaGithubSquare />
          </a>
        </motion.div>
      </div>
    </section>
  )
}
