'use client'

import React from 'react'
import SectionHeading from './section-heading'
import { motion } from 'framer-motion'
import { useSectionInView } from '@/lib/hooks'

export default function About() {
  const { ref } = useSectionInView('About')

  return (
    <motion.section
      ref={ref}
      className="mb-28 max-w-[45rem] text-center leading-8 sm:mb-40 scroll-mt-28"
      initial={{ opacity: 0, y: 100 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.175 }}
      id="about"
    >
      <SectionHeading>About me</SectionHeading>
      <p className="mb-3">
        I am a seasoned <span className="font-medium">Full Stack Engineer</span>{' '}
        with more than two years of professional experience and three years of
        freelancing in programming. My expertise spans{' '}
        <span className="italic">front-end</span> and{' '}
        <span className="italic">back-end development</span>, enabling me to
        craft holistic digital solutions. I'm committed to staying updated with
        industry trends and delivering innovative, user-focused products. With a
        track record of successful projects, I bring not only technical
        proficiency but also effective teamwork and communication skills to
        every endeavor. My passion for web development continues to drive me to
        create exceptional digital experiences.
      </p>

      <p>
        <span className="italic">Beyond the world of coding,</span> I find joy
        in a variety of interests and hobbies. Whether it's immersing myself in
        the immersive realms of video games, getting lost in captivating movies,
        or strumming my guitar (still a work in progress, but I'm getting there
        😉), I relish every moment of these pursuits. My{' '}
        <span className="font-medium">curiosity</span> extends beyond the
        familiar, and I'm always eager to dive into new horizons. Currently, I'm
        engrossed in exploring the cutting-edge technologies that are propelling
        us into the exciting world of outer space. 🚀✨
      </p>
    </motion.section>
  )
}
