import React from 'react'
import { CgWorkAlt } from 'react-icons/cg'
import { FaReact } from 'react-icons/fa'
import { LuGraduationCap } from 'react-icons/lu'
import vgoTruck from '@/public/vgotrucks.png'
import chatty from '@/public/chatty.png'
import uexBet from '@/public/uexbet.png'

export const links = [
  {
    name: 'Home',
    hash: '#home',
  },
  {
    name: 'About',
    hash: '#about',
  },
  {
    name: 'Projects',
    hash: '#projects',
  },
  {
    name: 'Skills',
    hash: '#skills',
  },
  {
    name: 'Experience',
    hash: '#experience',
  },
  {
    name: 'Contact',
    hash: '#contact',
  },
] as const

export const professions = [
  'Creative Developer',
  'UI/UX Designer',
  'Full Stack Engineer',
  'Mobile Application Developer',
]

export const experiencesData = [
  {
    title: 'Software Engineer',
    location: 'Chandigarh, IN',
    description:
      "I'm a full-stack developer working as a freelancer and I have build large and scalable applications with cutting edge technology in web as well as in mobile applications",
    icon: React.createElement(CgWorkAlt),
    date: '2019 - Present',
  },
  {
    title: 'Software Engineer',
    location: 'Chandigarh, IN',
    description:
      'I worked as a Software Engineer for 2 years in first job and I have build large and scalable applications with cutting edge technology',
    icon: React.createElement(CgWorkAlt),
    date: '2021 - 2023',
  },
  {
    title: 'Full-Stack Developer',
    location: 'Banglore, IN',
    description:
      "I'm working here as a full-stack developer. My stack includes React, Next.js, TypeScript, Tailwind, Postgress and MongoDB.",
    icon: React.createElement(FaReact),
    date: '2023 - present',
  },
] as const

export const projectsData = [
  {
    title: 'VGO Truck',
    description:
      'I dedicated two years of my career as an Android Developer to a startup project focused on providing truck services through a mobile application.',
    tags: ['Android', 'ReactJS', 'Java', 'RDBMS', 'Retrofit'],
    imageUrl: vgoTruck,
    url: 'https://vgotruck.web.app/',
  },
  {
    title: 'Chatty',
    description:
      'During my learning phase, I developed a comprehensive full-stack project that enables users to engage in real-time chat conversations within the application.',
    tags: ['React', 'TypeScript', 'Next.js', 'Tailwind', 'Redux'],
    imageUrl: chatty,
    url: '',
  },
  {
    title: 'UEXBet',
    description:
      'A Cricket Betting Mobile Application that is build for android users. It has all the functionality that a normal app requires like payment gateway, secure API connection etc.',
    tags: ['Android', 'Java', 'Retrofit', 'Firebase', 'Razorpay'],
    imageUrl: uexBet,
  },
] as const

export const skillsData = [
  'HTML',
  'CSS',
  'JavaScript',
  'TypeScript',
  'React',
  'Next.js',
  'Node.js',
  'Java',
  'Android Application Development',
  'Flutter',
  'Git',
  'Tailwind',
  'MongoDB',
  'Redux',
  'Express',
  'OracleSQL',
] as const
