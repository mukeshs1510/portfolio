import { Route, Routes } from 'react-router-dom'
import MainBody from './components/MainBody/MainBody'

function App() {
  return (
    <Routes>
      <Route path="/" element={<MainBody />} />
    </Routes>
  )
}

export default App
