import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import gsap from 'gsap'

// Scene
export default class ThreeScene {
  constructor(canvas) {
    this.scene = new THREE.Scene()
    // const geometry = new THREE.SphereGeometry(3, 64, 64)
    const geometry = new THREE.TorusGeometry(10, 4, 24, 100)
    const material = new THREE.MeshStandardMaterial({
      color: '#00ff83',
      roughness: 0.5,
    })

    // add mesh
    this.mesh = new THREE.Mesh(geometry, material)
    this.scene.add(this.mesh)

    // add camera
    this.camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      0.1,
      100
    )
    this.camera.position.z = 60
    this.scene.add(this.camera)

    // light
    this.light = new THREE.PointLight(0xffffff, 1, 100)
    this.light.position.set(0, 10, 10)
    this.scene.add(this.light)

    this.renderer = new THREE.WebGL1Renderer({
      canvas: canvas,
      antialias: false,
    })
    this.renderer.setPixelRatio(2)

    this.controls = new OrbitControls(this.camera, canvas)
    this.controls.enableDamping = true
    this.controls.enablePan = false
    this.controls.enableZoom = false
    this.controls.autoRotate = true
    this.controls.autoRotateSpeed = 5

    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.animateProcess()
    this.update()
  }

  animateProcess() {
    const gs = gsap.timeline({ defaults: { duration: 1 } })
    gs.fromTo(this.mesh.scale, { z: 0, x: 0, y: 0 }, { z: 1, x: 1, y: 1 })
  }

  updateValue(value) {
    //
  }
  onMouseMove(event, mouseDown, rgb) {
    if (mouseDown) {
      rgb = [
        Math.round((event.pageX / window.innerHeight) * 255),
        Math.round((event.pageX / window.innerWidth) * 255),
        138,
      ]
      //   animate
      let newColor = new THREE.Color(`rgb(${rgb.join(',')})`)
      gsap.to(this.mesh.material.color, {
        r: newColor.r,
        g: newColor.g,
        b: newColor.b,
      })
    }
  }
  onWindowResize(vpW, vpH) {
    this.camera.aspect = vpW / vpH
    this.camera.updateProjectionMatrix()
    this.renderer.setSize(vpW, vpH)
  }
  update(t) {
    this.controls.update()
    this.renderer.render(this.scene, this.camera)
    window.requestAnimationFrame(this.update.bind(this))
  }
}

// renderer
// const canvas = document.querySelector('.sphere')
// const renderer = new THREE.WebGL1Renderer({ canvas })

// renderer.setSize(800, 600)
// renderer.render(scene, camera)
