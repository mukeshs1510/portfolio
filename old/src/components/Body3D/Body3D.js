import React, { createRef, Component } from 'react'
import Threejs from '../Threejs/Threejs'
import './BodyStyle.scss'

export default class Body3D extends Component {
  constructor(props) {
    super(props)
    this.canvasRef = createRef()
  }

  // components lifecycle
  componentDidMount() {
    const ref = this.canvasRef.current
    this.viewGL = new Threejs(ref)
    window.addEventListener('mousemove', this.mouseMove)
    window.addEventListener('resize', this.handleResize)
  }

  componentDidUpdate(prevProps, prevState) {
    //
  }

  componentWillUnmount() {
    this.mouseDown = false
    this.rgb = []
    window.addEventListener('mousedown', () => (this.mouseDown = true))
    window.addEventListener('mouseup', () => (this.mouseDown = false))

    window.removeEventListener('mousemove', this.mouseMove)
    window.removeEventListener('resize', this.handleResize)
  }
  mouseMove = (event) => {
    this.viewGL.onMouseMove(event, this.mouseDown, this.rgb)
  }

  handleResize = () => {
    this.viewGL.onWindowResize(window.innerWidth, window.innerHeight)
  }

  render() {
    return (
      <div className="gradient">
        <canvas ref={this.canvasRef} id="sphere"></canvas>
        <h1 className="text-white name_center">Mukesh Suthar</h1>
      </div>
    )
  }
}
