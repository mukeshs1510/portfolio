import React from 'react'
import Sidebar from '../SideBar/SideBar'
import './LayoutStyles.scss'

const Layout = () => {
  return (
    <>
      <Sidebar />
    </>
  )
}

export default Layout
