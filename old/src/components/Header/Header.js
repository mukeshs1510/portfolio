import React from 'react'
import { Link } from 'react-router-dom'
import './HeaderStyles.scss'

const Header = () => {
  return (
    <header class="text-gray-400 bg-black body-font">
      <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <nav class="flex lg:w-2/5 flex-wrap items-center text-base md:ml-auto gap-5">
          <Link to="/" class="hover:text-white">
            Home
          </Link>
          <Link to="/" class="hover:text-white">
            ABout
          </Link>
          <Link to="/" class="hover:text-white">
            Services
          </Link>
          <Link to="/" class="hover:text-white">
            Skills
          </Link>
          <Link to="/" class="hover:text-white">
            Projects
          </Link>
        </nav>
        <Link
          to="/"
          class="flex order-first lg:order-none lg:w-1/5 title-font font-medium items-center text-white lg:items-center lg:justify-center mb-4 md:mb-0"
        >
          <span class="ml-3 text-5xl xl:block lg:hidden">
            PortFo<span className="text-teal-400 ">lio</span>
          </span>
        </Link>
        <div class="lg:w-2/5 inline-flex lg:justify-end ml-5 lg:ml-0">
          <button class="inline-flex items-center bg-gray-800 border-0 py-1 px-3 focus:outline-none hover:bg-gray-700 rounded text-base mt-4 md:mt-0">
            Hire me
            <svg
              fill="none"
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              class="w-4 h-4 ml-1"
              viewBox="0 0 24 24"
            >
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </button>
        </div>
      </div>
    </header>
  )
}

export default Header
