import React from 'react'
import { Link } from 'react-router-dom'
import './SidebarStyles.scss'

const Sidebar = () => {
  return (
    <div className="nav">
      <Link className="logo" to="/">
        mukesh.
      </Link>
    </div>
  )
}

export default Sidebar
