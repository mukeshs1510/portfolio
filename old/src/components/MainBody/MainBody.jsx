import React from 'react'
import Body3D from '../Body3D/Body3D'
import Header from '../Header/Header'
import './MainBodyStyle.scss'

const MainBody = () => {
  return (
    <div className="mainbody_container">
      <Header />
      <Body3D />
    </div>
  )
}

export default MainBody
