'use client'
import { Inter } from 'next/font/google'

import Header from '@/components/header'
import './globals.css'
import ActiveSectionContextProvider from '@/context/active-section-context'
import Footer from '@/components/footer'
import ThemeSwitch from '@/components/theme-switch'
import ThemeContextProvider from '@/context/theme-context'
import { Toaster } from 'react-hot-toast'
import { Cursor } from '@/components/cursor'
import { useSpring, animated } from 'react-spring'

const inter = Inter({ subsets: ['latin'] })

const metadata = {
  title: 'Mukesh | Software Engineer',
  description: 'A full-stack developer.',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  const [animatedProps, setAnimatedProps] = useSpring(() => ({
    transform: `translate3d(0px, 0px, 0)`,
  }))

  function handleMouseMove(
    event: React.MouseEvent<HTMLBodyElement, MouseEvent>
  ): any {
    setAnimatedProps({
      transform: `translate3d(${event.clientX}px, ${event.clientY}px, 0)`,
    })
  }

  return (
    <html lang="en" className="!scroll-smooth">
      <body
        onMouseMove={handleMouseMove}
        className={`${inter.className} bg-gray-50 text-gray-950 relative pt-28 sm:pt-36 dark:bg-gray-900 dark:text-gray-50 dark:text-opacity-90`}
      >
        <div className="bg-[#fbe2e3] absolute top-[-6rem] -z-10 right-[11rem] h-[31.25rem] w-[31.25rem] rounded-full blur-[10rem] sm:w-[68.75rem] dark:bg-[#946263]"></div>
        <div className="bg-[#dbd7fb] absolute top-[-1rem] -z-10 left-[-35rem] h-[31.25rem] w-[50rem] rounded-full blur-[10rem] sm:w-[68.75rem] md:left-[-33rem] lg:left-[-28rem] xl:left-[-15rem] 2xl:left-[-5rem] dark:bg-[#676394]"></div>
        <div className="containerCursor">
          <animated.div style={animatedProps} className="wrapperCursor">
            <Cursor />
          </animated.div>
        </div>
        <ThemeContextProvider>
          <ActiveSectionContextProvider>
            <Header />
            {children}
            <Footer />

            <Toaster position="top-right" />
            <ThemeSwitch />
          </ActiveSectionContextProvider>
        </ThemeContextProvider>
      </body>
    </html>
  )
}
